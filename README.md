# Wepay

轻量的微信支付组件(A Lightweight Wechat Pay Component)
---

+ 包引入:
	
	```xml
	<dependency>
        <groupId>me.hao0</groupId>
        <artifactId>wepay-core</artifactId>
        <version>1.2.4</version>
    </dependency>
	```
	
+ 具体文档见<a target="_blank" href="https://github.com/ihaolin/wepay">这里</a>。
        